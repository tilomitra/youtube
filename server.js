//setup Dependencies
var http    = require('http'),
    fs      = require('fs'),
    connect = require('connect'),
    express = require('express'),
    exphbs  = require('express3-handlebars'),
    state   = require('express-state'),
    app     = express(),
    port    = (process.env.PORT || 7000),
    request = require('request'),
    server  = app.listen(port, 'localhost'),

    //remove this if you don't need socket.io
    io = require('socket.io').listen(server);

var CLIENT_ID = '488901316074.apps.googleusercontent.com',
    CLIENT_SECRET = '2selL_1do2YVUz042woIm9fS',
    REDIRECT_URL = 'http://localhost:7000/oauth2callback',
    YOUTUBE_KEY = 'AI39si5fXtYN3tcbVialU3ODmJiqHF3v3l0sWkrgw25YG7gIYNZzKVAc1LHnpzag9SPPlBp8gk36wKR4DpfBNgb8WeTQRR-FVg',
    OAUTHCLIENT;

//Setup Express App
state.extend(app);
app.engine('handlebars', exphbs({defaultLayout: 'main'}));
app.set('view engine', 'handlebars');
app.enable('view cache');
app.enable('strict routing');

//Change "ProjectName" to whatever your application's name is.
app.set('state namespace', 'ProjectName');

//Create an empty Data object and expose it to the client. This
//will be available on the client under ProjectName.Data
app.expose({}, 'Data');

app.configure(function(){
    app.set('views', __dirname + '/views');
    app.use(connect.bodyParser());
    app.use(express.cookieParser());
    app.use(express.session({ secret: "shhhhhhhhh!"}));
    app.use(connect.static(__dirname + '/static'));
    app.use(app.router);
    app.use(function(err, req, res, next){
      // if an error occurs Connect will pass it down
      // through these "error-handling" middleware
      // allowing you to respond however you like
      if (err instanceof NotFound) {
          res.render('404');
      }
      else {
          res.render('500');
      }
    })
});

//Setup Socket.IO
//You can remove this whole chunk if you don't want socket.io
io.sockets.on('connection', function(socket){
  console.log('Client Connected');
  socket.on('message', function(data){
    socket.broadcast.emit('app_message',data);
    socket.emit('app_message',data);
  });
  socket.on('disconnect', function(){
    console.log('Client Disconnected.');
  });
});


///////////////////////////////////////////
//              Routes                   //
///////////////////////////////////////////

/////// ADD ALL YOUR ROUTES HERE  /////////
app.get('/', function (req, res, next) {

    var googleapis = require('googleapis'),
        OAuth2 = googleapis.auth.OAuth2;

    OAUTHCLIENT = new OAuth2(CLIENT_ID, CLIENT_SECRET, REDIRECT_URL);

    // generates a url that allows offline access and asks permissions
    // for Google+ scope.
    var url = OAUTHCLIENT.generateAuthUrl({
      access_type: 'offline',
      scope: 'https://gdata.youtube.com'
    });

    res.redirect(url);
    //res.render('home');
});


app.get('/oauth2callback', function (req, res) {

  var code = req.query.code;

  OAUTHCLIENT.getToken(code, function(err, tokens) {
    /*
    { access_token: 'ya29.1.AADtN_UEGvphUOnhvKEH5cWQvZNddn9adQfIMEeirJPaYwSpcFGYHW6lTVzFAgg',
      token_type: 'Bearer',
      expires_in: 3600,
      refresh_token: '1/Ta0qOIXSn1q9tu5J7b2Jp91O1QdsVielIaTFjynGHP0' }
    */

    var accesstoken = tokens.access_token;

    console.log('in here');
    // contains an access_token and optionally a refresh_token.
    // save them permanently.

    var file_reader = fs.createReadStream('./static/test.mov', {encoding: 'binary'});
    var title = 'TEST';
    var description = 'test description';
    var category = 'Travel';
    var keywords = 'test';
    var file_contents = '';
    file_reader.on('data', function(data) {
        console.log('reading video...');
        file_contents += data;
    });
    file_reader.on('end', function()
    {
        console.log('finished reading video...');
        var xml =
            '<?xml version="1.0"?>' +
            '<entry xmlns="http://www.w3.org/2005/Atom" xmlns:media="http://search.yahoo.com/mrss/" xmlns:yt="http://gdata.youtube.com/schemas/2007">' +
            '   <media:group>' +
            '       <media:title type="plain">' + title + '</media:title>' +
            '       <media:description type="plain">' + description + '</media:description>' +
            '       <media:category scheme="http://gdata.youtube.com/schemas/2007/categories.cat">' + category + '</media:category>' +
            '       <media:keywords>' + keywords + '</media:keywords>' +
            '   </media:group>' +
            '</entry>';

        /* Copy/pasted from here... */

        var boundary = Math.random();
        var post_data = [];
        var part = '';

        part = "--" + boundary + "\r\nContent-Type: application/atom+xml; charset=UTF-8\r\n\r\n" + xml + "\r\n";
        post_data.push(new Buffer(part, "utf8"));

        part = "--" + boundary + "\r\nContent-Type: video/mov\r\nContent-Transfer-Encoding: binary\r\n\r\n";
        post_data.push(new Buffer(part, 'ascii'));
        post_data.push(new Buffer(file_contents, 'binary'));
        post_data.push(new Buffer("\r\n--" + boundary + "--"), 'ascii');

        var post_length = 0;
        for(var i = 0; i < post_data.length; i++)
        {
            console.log('inside for loop...');
            post_length += post_data[i].length;
        }

        /* ...to here */

        var options = {
          host: 'uploads.gdata.youtube.com',
          port: 80,
          path: '/feeds/api/users/default/uploads?alt=json',
          method: 'POST',
            headers: {
                'Authorization': 'Bearer ' + accesstoken,
                'GData-Version': '2',
                'X-GData-Key': 'key=' + YOUTUBE_KEY,
                'Slug': 'test.mov',
                'Content-Type': 'multipart/related; boundary="' + boundary + '"',
                'Content-Length': post_length,
                'Connection': 'close'
            }
        }

        console.log(file_contents);

        //https://developers.google.com/youtube/2.0/developers_guide_protocol_direct_uploading#Direct_uploading
        console.log(options);
        console.log('done setting options. about to upload..');

        var req = http.request(options, function(res) {
          console.log('in here');
            res.setEncoding('utf8');

            var response = '';
            res.on('data', function(chunk)
            {
              console.log('chunk');
                response += chunk;
            });
            res.on('end', function()
            {
                console.log(response);
                response = JSON.parse(response);

                callback(response);
            });
        });

        for (var i = 0; i < post_data.length; i++)
        {
            req.write(post_data[i]);
        }

        req.on('error', function(e) {
          console.error(e);
        });

        //req.end();
    });
  });
});

//A Route for Creating a 500 Error (Useful to keep around)
app.get('/500', function(req, res){
    throw new Error('This is a 500 Error');
});

//The 404 Route (ALWAYS Keep this as the last route)
app.get('/*', function(req, res){
    throw new NotFound;
});

function NotFound(msg){
    this.name = 'NotFound';
    Error.call(this, msg);
    Error.captureStackTrace(this, arguments.callee);
}

console.log('Listening on http://localhost:' + port );
